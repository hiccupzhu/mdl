#!/usr/bin/env python
#coding=utf-8

import os
import sys
import re
import json

section_text = """\
[general]#section name
width  = 1920;
height = 1080;
video_format = YV12;

#######################################################
[command]
#filesrc location=/opt/CH3source.ts ! queue ! mpegtsdemux name=demuxer 
#demuxer.video_0121 ! queue ! mpegtsmux name=muxer ! queue ! filesink location=/opt/CH3source.new.ts 
# demuxer.audio_0131 ! queue ! muxer.
filesrc location=/opt/CH3source.ts ! queue ! mpegtsdemux name=demuxer
demuxer.video_0121 ! queue ! appsink >> @video
demuxer.audio_0131 ! queue ! appsink >> @audio
@video >> appsrc ! queue ! mpegtsmux name=muxer ! queue ! filesink location=/opt/CH3source.new.ts
@audio >> appsrc ! queue ! muxer. 

""";

section_command_text = """
src_bio = {
    udpsrc uri=udp://127.0.0.1:20000 ! queue ! timefilter cachesize=0x40000 ! queue ! mpegtsdemux name=demuxer
    demuxer.video_01c3 ! queue ! h264dec ! queue ! ffmpegcolorspace ! caps = video/x-raw-yuv,format=(fourcc)I420 ! queue ! deinterlace method=4 fields=1 ! queue ! mscenecut keyint=250 ! queue ! appsink >> @video
    demuxer.audio_01c4 ! queue ! mad ! queue ! audioconvert ! queue ! audioresample ! audio/x-raw-int,width=16,depth=16,rate=48000,channels=2,endianness=1234 ! queue ! appsink >> @audio
    demuxer.private_01c9 ! private/x-dvbsub,language=chi ! queue ! appsink >> @subtile
}
enc_bio = {
    @video >> appsrc ! queue ! mvideoscale ! video/x-raw-yuv,width=720,height=576 ! queue ! x264enc byte-stream=TRUE ! queue ! mpegtsmux name=muxer ! queue ! filesink location=/opt/bio_itv.ts
    @audio >> appsrc ! queue ! faac name=faac1 outputformat=1 ! queue ! muxer.
    @subtitle >> appsrc ! queue ! muxer.
}
""";

general_text = """\
tcp_port = 8000;
stream_port = 20120;
width  = 1920;
height = 1080;
video_format = YV12;
""";

    
def mdlSyntaxParse(text):
    sections = {};
##    remove the config-file comment
    text, num = re.subn("#.*", "", text)
##    remove the config-file redundant '\n'
    text, num = re.subn("[\n]+", "\n", text)

    sec_syntax = "[a-zA-Z][\w\-]*"
    
    mm = re.split("\[(%s)\]\s" % (sec_syntax), text);
##    print len(mm), mm;
##    i = 1 means skip the '0' index
    i = 1;
    while i < len(mm):
        key = mm[i];
        text = mm[i + 1];
        sections[key] = text;

        i += 2;
    return sections;

def mdlSyntaxFromeFile(filename):
    f = open(filename, "r");
    text = f.read(-1);
    f.close();
    return mdlSyntaxParse(text);

def mdlSyntaxParseCommand(text):
    sections = {};
##    print text;

    sec_syntax_name = "\s*([a-zA-z]+_[a-zA-z0-9]+)\s*=\s*";
    sec_syntax_context = "{([^{^}]*)}";
    sec_syntax = "%s%s" % (sec_syntax_name, sec_syntax_context)

    print "#### sec_syntax=", sec_syntax
    for mmm in re.finditer(sec_syntax, text):
        if mmm:
            for i in range(1, len(mmm.groups())):
                key, text = mmm.groups(i);
                sections[key] = text.strip();
##              print "    mmm.groups(%d)#### %s" % (i, mmm.groups(i))
        else:
            print "ERROR: ========="


    keys = sections.keys();
    src_name = "";
    for key in keys:
        mm = re.match("([\w^_]+)_([\w^_]+)", key);
        if ((not mm) or (len(mm.groups()) != 2)):
            return {"error": "----"};
        else:
            key_type = mm.group(1);
            if not key_type in ("src", "enc"):
                return {"error":'''ERROR, key_type[%s] NOT-IN ("src", "enc")''' % (key_type)};
            if src_name == "":
                src_name = mm.group(2);
            else:
                if src_name != mm.group(2):
                    print "ERROR, name[%s] NOT-match src_name[%s]" % (mm.group(2), src_name);
                    return {"error":"ERROR, name[%s] NOT-match src_name[%s]" % (mm.group(2), src_name)};
                else:
                    pass;
        
    return sections;


def mdlSyntaxParseGeneral(text):
    dic = {};

##    text, num = re.subn("\s+", " ", text);

##    print "outstr=%s, num=%d" %(text, num);

    var_txt_list = [];
    for var_txt in text.split(";"):
        var = var_txt.strip();
        if(var == ""):
            continue;
        var_txt_list.append(var);

    print var_txt_list;
    
    for var_txt in var_txt_list:
        mm = re.match("([\w]+)\s*=\s*([\w]+)\s*$", var_txt);
        if not mm:
            print '*** ERROR: Syntax unknown "%s"' % (var_txt);
            return {};
        else:
            key = mm.group(1);
            value = mm.group(2);
            dic[key] = value;
    return dic;

def mdlSyntaxMergeVariable(var_dic, text):
    return text;

    
if __name__ == "__main__":
##    sections = mdlSyntaxFromeFile("./launch.mdl");
##    sections = mdlSyntaxParseCommand(section_command_text);
    sections = mdlSyntaxParseGeneral(general_text);
    print "=====================PrintDic============================"
    for (key, value) in sections.items():
        print """[%s]:%s""" % (key, value)
