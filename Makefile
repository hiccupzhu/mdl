CC=gcc -g
PWD=$(shell pwd)

CFLAGS=-DPACKAGE_NAME='"gslanguage"' \
       -DGST_PACKAGE_ORIGIN='"http://gslanguage.com"' \
       -DGETTEXT_PACKAGE='"gslanguage"'\
       $(shell pkg-config --cflags gstreamer-app-0.10) \
       $(INCLUDE)

LDFLAGS=$(shell pkg-config --libs gstreamer-app-0.10) -lpython2.6

INCLUDE=-I/usr/include/gstreamer-0.10/gst -I./src
#INCLUDE=-I/usr/local/include/gstreamer-0.10/gst
VPATH = ./src

MDL_SRC=grammar.tab.c \
        parse.yy.c \
        mdl.c \
        mdl_file.c \
        mdl_buffer.c \
        mdl_syntax_py.c
#        section.tab.c \
#        section.yy.c \

MDL_OBJ=$(patsubst %.c,%.o,$(MDL_SRC))

LAUNCH_SRC=grammar.tab.c \
        parse.yy.c \
        gst-launch.c
LAUNCH_OBJ=$(patsubst %.c,%.o,$(LAUNCH_SRC))

.PHONY: all clean install

#all:gst-launch mdl
all:mdl
install:mdl_install

gst-launch:$(LAUNCH_OBJ)
	$(CC) $(LDFLAGS) -o $@ $+

gst-launch.o:gst-launch.c
	$(CC) $(CFLAGS) -c -o $@ $+
	
mdl: $(MDL_OBJ)
	$(CC) $(LDFLAGS) -o $@ $^
mdl_file.o:mdl_file.c
	$(CC) $(CFLAGS) -c -o $@ $+
section.tab.o:section.tab.c
	$(CC) $(CFLAGS) -c -o $@ $+
section.yy.o:section.yy.c
	$(CC) $(CFLAGS) -c -o $@ $+
mdl.o:mdl.c
	$(CC) $(CFLAGS) -c -o $@ $+
section.yy.c:section.l
	flex --debug -Pmdl_section_ -o $@ $<
section.tab.c:section.y
	bison --debug -pmdl_section_  -v -d $<
grammar.tab.o:grammar.tab.c
	$(CC) $(CFLAGS) -c -o $@ $+
parse.yy.o:parse.yy.c
	$(CC) $(CFLAGS) -c -o $@ $+
parse.yy.c:parse.l
	flex --debug -Pmdl_cmd_ -o $@ $<
grammar.tab.c:grammar.y
	bison --debug -d -pmdl_cmd_ $<

########################################################################
mdl_install:
	install ./mdl /usr/bin


########################################################################
clean:mdl_clean gst-launch_clean

gst-launch_clean:
	rm -rf gst-launch grammar.tab.* parse.yy.* gst-launch.o

mdl_clean:
	rm -rf section.tab.* mdl.o section.yy.* mdl section.output mdl_file.o *.o