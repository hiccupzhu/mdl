/*
 * mdl_buffer.c
 *
 *  Created on: Nov 12, 2013
 *      Author: szhu
 */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "mdl_buffer.h"
#include "gdef.h"

void
mdl_buffer_alloc_w8(GsByte* b, char ch)
{
    int stride = 32;

    if(b->mBufPtr >= b->mBufEnd){
        int sz = 0;
        sz = b->mBufEnd - b->mBuf;

        b->mBuf = (uint8_t*)realloc(b->mBuf, sz + stride);
        b->mBufPtr = b->mBuf + sz;
        b->mBufEnd = b->mBuf + sz + stride;

    }

    assert(b->mBufPtr < b->mBufEnd);
    *b->mBufPtr = ch;
    b->mBufPtr ++;

    return;
}

