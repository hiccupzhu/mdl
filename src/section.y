%{
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "mtype.h"

#define YYERROR_VERBOSE 1
#define YYLEX_PARAM scanner
#define PRINT(fmt, ...) printf("  ##section.y:" fmt "\n", ##__VA_ARGS__)

typedef void* yyscan_t;


static int yyerror (void *scanner, section_t *sec, const char *s);


%}

%union {
    char *s;
    section_t *sec;
}

%token <s> ID_SECTION
%token <s> ID_CONTEXT
%type <sec> section

%parse-param { void *scanner }
%parse-param { void *sec }
%pure-parser

%start section
%%

section: ID_SECTION
    {
        int nb;
        PRINT("[section: ID_SECTION]");
        nb = $$->nb_sections;
        ($$)->section[nb] = strdup($1);
        free($1);
    }
    | ID_CONTEXT
    {
        int nb;
        PRINT("[section: ID_CONTEXT]");
        nb = $$->nb_sections;
        PRINT("[section: ID_CONTEXT]nb=%d", nb);
        ($$)->context[nb] = strdup($1);
        ($$)->nb_sections ++;
        free($1);
    }

section:section
{
    printf("###[section:section]\n");
}

%%

int
mdl_section_parse_launch(const char* _dstr)
{
    section_t sec;
    yyscan_t scanner;
    
    char* dstr = strdup(_dstr);
    memset(&sec, 0, sizeof(section_t));
    
    mdl_section_lex_init(&scanner);
    mdl_section__scan_string (dstr, scanner);
    
    if (yyparse (scanner, &sec) != 0) {
        mdl_section_lex_destroy (scanner);
        free (dstr);
      	printf("## ERROR!!\n");
        exit(0);
    }
    mdl_section_lex_destroy (scanner);
    
    free (dstr);
    printf("Successful !!!\n");
    
    return 0;
}

static int
yyerror (void *scanner, section_t *sec, const char *s)
{
  /* FIXME: This should go into the GError somehow, but how? */
  printf ("Error during parsing: %s", s);
  return -1;
}