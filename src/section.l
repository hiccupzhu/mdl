%{
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "mtype.h"
#include "section.tab.h"

#define PRINT(fmt, ...) printf("  ##section.l:" fmt "\n", ##__VA_ARGS__)
%}

_crlf [\n]
_identifier [[:alpha:]][[:alnum:]\-_%:]*
_id_section "["{_identifier}+"]"
_id_context (?:{_id_section}).*(?:{_id_section})


%option noyywrap
%option nounput
%option noinput
%option bison-bridge
%option never-interactive
%option reentrant
%%

{_id_section} {
    PRINT ("SECTION: %s", yytext);
    yylval->s = strdup(yytext);
    return ID_SECTION;
}

{_crlf} {
    PRINT ("CRLF");
}

{_id_context} {
    PRINT ("CONTEXT: %s", yytext);
    yylval->s = strdup(yytext);
    return ID_CONTEXT;
}

. {
    PRINT("ERROR  Invalid flex:%s", yytext);
}
%%