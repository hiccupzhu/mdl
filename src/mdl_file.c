/*
 * mdl_file.c
 *
 *  Created on: Nov 1, 2013
 *      Author: szhu
 */
#include <string.h>
#include <stdlib.h>

#include "mdl_file.h"
#include "gdef.h"
#include "mdl_buffer.h"

// ###################################################################################


// ###################################################################################

mdl_file_t*
mdl_file_new0(const char* filename, const char* mode)
{
    int ret;
    mdl_file_t* mdl;
    mdl = (mdl_file_t*)calloc(sizeof(mdl_file_t), 1);
    if(mdl == NULL) return NULL;
    ret = mdl_file_open(mdl, filename, mode);
    if(ret < 0){
        goto failed;
    }

    return mdl;
failed:
    free(mdl);
    return NULL;
}

int
mdl_file_open(mdl_file_t* mdl, const char* filename, const char* mode)
{
    mdl->fp = fopen(filename, mode);
    if(mdl->fp == NULL) return -1;
    mdl->filename = strdup(filename);

    return 0;
}

int
mdl_file_close(mdl_file_t* mdl)
{
    if(mdl->fp){
        fclose(mdl->fp);
        mdl->fp = NULL;
    }
    SAFE_FREE(mdl->filename);
}

char*
mdl_file_readline(mdl_file_t* mdl)
{
    char ch;
    GsByte b;

    memset(&b, 0, sizeof(GsByte));

    while( !mdl_file_eof(mdl) ){
        int ret;
        ret = fread(&ch, 1, 1, mdl->fp);
        if(ret <= 0) break;

        mdl_buffer_alloc_w8(&b, ch);

        if(ch == '\n') break;
    }

    if(b.mBufPtr != b.mBuf) mdl_buffer_alloc_w8(&b, 0);

    return b.mBuf;
}

int
mdl_file_size(mdl_file_t* mdl)
{
    int filesize;
    FILE* fp = mdl->fp;
    fseek(fp, 0, SEEK_END);
    filesize = ftello(fp);
    fseek(fp, 0, SEEK_SET);

    return filesize;
}

int
mdl_file_eof(mdl_file_t* mdl)
{
    return feof(mdl->fp);
}


