/*
 * mdl_file.h
 *
 *  Created on: Nov 1, 2013
 *      Author: szhu
 */

#ifndef MDL_FILE_H_
#define MDL_FILE_H_

#include <stdio.h>

typedef struct{
    FILE* fp;
    char* filename;
}mdl_file_t;

int     mdl_file_open(mdl_file_t* mdl, const char* filename, const char* mode);
int     mdl_file_close(mdl_file_t* mdl);
char*   mdl_file_readline(mdl_file_t* mdl);
int     mdl_file_size(mdl_file_t* mdl);
int     mdl_file_eof(mdl_file_t* mdl);

#endif /* MMDL_FILE_H_ */
