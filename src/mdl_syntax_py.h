/*
 * mdl_syntax_py.h
 *
 *  Created on: Nov 15, 2013
 *      Author: szhu
 */

#ifndef MDL_SYNTAX_PY_H_
#define MDL_SYNTAX_PY_H_

#include <python2.6/Python.h>
#include <gst/gst.h>

int        mdl_syntax_init();
void       mdl_syntax_uninit();
GHashTable *mdl_syntax_frome_file(const char* filename);
GHashTable *mdl_syntax_parse_command(const char* command);
GHashTable *mdl_syntax_parse_general(const char* command);

#endif /* MDL_SYNTAX_C_H_ */
