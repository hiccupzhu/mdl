/*
 * mdl_syntax_py.c
 *
 *  Created on: Nov 15, 2013
 *      Author: szhu
 */

#include "mdl_syntax_py.h"
#include "gdef.h"
#include "types.h"


/////////////////////////////////////////////////////////////////////////////
static  PyObject* pMod    = NULL;
static  PyObject* pParm   = NULL;
static  PyObject* pRetDic = NULL;

/////////////////////////////////////////////////////////////////////////////
static void free_key(gpointer data);
static void free_value(gpointer data);

/////////////////////////////////////////////////////////////////////////////
int
mdl_syntax_init()
{
    Py_Initialize();
    if(!Py_IsInitialized())
    {
        return -1;
    }

    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");

    if( !(pMod = PyImport_ImportModule("mdl_syntax") ) ){
        av_print("ERROR:Load mdl_syntax.py FAILED !!!\n");
        return -1;
    }

    return 0;
}

void
mdl_syntax_uninit()
{
    /*
     * PyImport_AddModule returns a borrowed reference.
     * Which means you're not allowed to call Py_DECREF at all
     * unless you have actually taken control over it
     * (for example by increasing the reference count via Py_INCREF)
     */
//    Py_DECREF(pRetDic);
//    Py_DECREF(pParm);
//    Py_DECREF(pFunc);
//    Py_DECREF(pMod);

    Py_Finalize();
}

GHashTable *
mdl_syntax_frome_file(const char* filename)
{
    GHashTable *table = NULL;
    PyObject   *pFunc = NULL;

    if( !(pFunc = PyObject_GetAttrString(pMod, "mdlSyntaxFromeFile") ) ){
        av_print("ERROR:Get funtion[mdlSyntaxFromeFile] FAILED !!!\n");
        exit(0);
    }

    pParm = Py_BuildValue("(s)", filename);
    pRetDic = PyEval_CallObject( pFunc, pParm);



    table = g_hash_table_new_full(g_str_hash, g_str_equal, free_key, free_value);

    PyObject *pKey, *pValue;
    Py_ssize_t pos = 0;
    while (PyDict_Next(pRetDic, &pos, &pKey, &pValue)) {
        char *key, *value;

        PyArg_Parse( pKey, "s", &key );
        PyArg_Parse( pValue, "s", &value );

//        av_print("[%s] :%s\n", key, value);

        g_hash_table_insert(table, strdup(key), strdup(value));

//        Py_DECREF(pKey);
//        Py_DECREF(pValue);
    }


//    char *key, *value;
//    GHashTableIter iter;
//    g_hash_table_iter_init (&iter, table);
//    while(g_hash_table_iter_next(&iter, &key, &value)){
//        av_print("#[%s]\n", key);
//        av_print("#%s\n", value);
//    }


    return table;
}

GHashTable *
mdl_syntax_parse_command(const char* command)
{
    GHashTable *table = NULL;
    PyObject   *pFunc = NULL;

    if( !(pFunc = PyObject_GetAttrString(pMod, "mdlSyntaxParseCommand") ) ){
        av_print("ERROR:Get funtion[mdlSyntaxParseCommand] FAILED !!!\n");
        exit(0);
    }
    pParm = Py_BuildValue("(s)", command);
    pRetDic = PyEval_CallObject( pFunc, pParm);

    table = g_hash_table_new_full(g_str_hash, g_str_equal, free_key, free_value);

    PyObject *pKey, *pValue;
    Py_ssize_t pos = 0;
    while (PyDict_Next(pRetDic, &pos, &pKey, &pValue)) {
        char *key, *value;

        PyArg_Parse( pKey, "s", &key );
        PyArg_Parse( pValue, "s", &value );

//        av_print("[%s] :%s\n", key, value);

        g_hash_table_insert(table, strdup(key), strdup(value));

//        Py_DECREF(pKey);
//        Py_DECREF(pValue);
    }

    return table;
}

GHashTable *
mdl_syntax_parse_general(const char* command)
{
    GHashTable *table = NULL;
    PyObject   *pFunc = NULL;

    av_print("\n");

    if( !(pFunc = PyObject_GetAttrString(pMod, "mdlSyntaxParseGeneral") ) ){
        av_print("ERROR:Get funtion[mdlSyntaxParseGeneral] FAILED !!!\n");
        exit(0);
    }
    av_print("\n");
    pParm = Py_BuildValue("(s)", command);
    pRetDic = PyEval_CallObject( pFunc, pParm);

    av_print("\n");

    table = g_hash_table_new_full(g_str_hash, g_str_equal, free_key, free_value);

    PyObject *pKey, *pValue;
    Py_ssize_t pos = 0;
    while (PyDict_Next(pRetDic, &pos, &pKey, &pValue)) {
        char *key, *value;

        PyArg_Parse( pKey, "s", &key );
        PyArg_Parse( pValue, "s", &value );

//        av_print("[%s] :%s\n", key, value);

        g_hash_table_insert(table, strdup(key), strdup(value));

//        Py_DECREF(pKey);
//        Py_DECREF(pValue);
    }

    return table;
}

char*
mdl_syntax_merge_variable(GHashTable *var_table, const char* command)
{

}




static void
free_key(gpointer data)
{
    free(data);
}

static void
free_value(gpointer data)
{
    free(data);
}
