/*
 * mmdl.c
 *
 *  Created on: Oct 31, 2013
 *      Author: szhu
 */

#include <stdio.h>
#include <gst/gst.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "gdef.h"
#include "mdl_file.h"
#include "tools.h"
#include "types.h"
#include "mdl_syntax_py.h"

//////////////////////////////////////////////////////////////////////////
typedef enum _EventLoopResult
{
  ELR_NO_ERROR = 0,
  ELR_ERROR,
  ELR_INTERRUPT
} EventLoopResult;

//////////////////////////////////////////////////////////////////////////
//int mdl_section_parse(const char* data);
GstElement *    mdl_cmd_parse_launch (const gchar *str, GError **error, GstParseContext *ctx, GstParseFlags flags);
static void     mdl_parse_args(int argc, char* argv[], GOptionEntry *options);
static EventLoopResult mdl_event_loop (GstElement * pipeline, gboolean blocking, GstState target_state);
static gboolean check_intr (GstElement * pipeline);
static GSList*  mdl_cmd_parse_text(const char* _text);
static void     sigint_restore (void);
static void     sigint_handler_sighandler (int signum);
static void     sigint_setup (void);
static void     free_key(gpointer data);
static void     free_value(gpointer data);
static gboolean mdl_file_exist(const char* filename);
static __pid_t  mdl_create_server_process(char* file, char* args);

//////////////////////////////////////////////////////////////////////////
static gboolean verbose = FALSE;
static gboolean is_live = FALSE;
static gboolean messages = FALSE;
static gboolean waiting_eos = FALSE;
static gboolean caught_intr = FALSE;
static EventLoopResult caught_error = ELR_NO_ERROR;


/////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{

    gchar *conffile = NULL;
    GHashTable *section_table;
    GOptionEntry options[] = {
            {"verbose",  'v', 0, G_OPTION_ARG_NONE, &verbose,      N_("Output status information and property notifications"), NULL},
            {"conffile", 'c', 0, G_OPTION_ARG_STRING, &conffile,   N_("Command read a config file"), NULL},
            GST_TOOLS_GOPTION_VERSION,
            {NULL}
    };
    GstElement *pipeline = NULL;
    GSList *pipelist = NULL;
    gint res = 0;
    GError *err = NULL;
    GstStateChangeReturn scret;
    GstParseContext parse_ctx = { NULL };
    char cmd[1024];
    int ret ;

    g_thread_init (NULL);

    mdl_parse_args(argc, argv, options);

    if(!mdl_file_exist(conffile)){
        av_print("*** ERROR: %s NOT EXIST!!!\n", conffile);
        exit(1);
    }

    //Init python
    ///////////////////////////////////////////////////////////////////////////////
    if(mdl_syntax_init() < 0){
        av_print("*** ERROR: mdl_syntax_init() FAILD!!!\n");
        exit(0);
    }

    section_table = mdl_syntax_frome_file(conffile);
    g_free(conffile);

    ///////////////////////////////////////////////////////////////////////////////

    sigint_setup();


    ///////////////////////////////////////////////////////////////////////////////

    GHashTable *gereral_table = NULL;
    {
        const char* cmd_str = g_hash_table_lookup(section_table, "general");
        if(cmd_str){
            gereral_table = mdl_syntax_parse_general(cmd_str);
        }else{
            av_print("*** ERROR: config can NOT found '[general]'\n");
//            goto exit;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////

    GSList *ls, *ll;
    GHashTable *cmd_table = NULL;

    {
        const char* cmd_str = g_hash_table_lookup(section_table, "command");
        if(cmd_str){
            cmd_table = mdl_syntax_parse_command(cmd_str);
        }else{
            cmd_str = g_hash_table_lookup(section_table, "command0");
            if(cmd_str){
                cmd_table = g_hash_table_new_full(g_str_hash, g_str_equal, free_key, free_value);
                g_hash_table_insert(cmd_table, strdup("command0"), strdup(cmd_str));
            }else{
                av_print("*** ERROR: config can NOT found '[command]' or '[command0]'\n");
                goto exit;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////


    GHashTableIter iter;
    char *pipe_name, *cmd_text;
    g_hash_table_iter_init (&iter, cmd_table);
    while(g_hash_table_iter_next(&iter, &pipe_name, &cmd_text)){
        av_print("#pipe_name=[%s]\n", pipe_name);
        av_print("#cmd_text=%s\n", cmd_text);

        err = NULL;

        pipeline = mdl_cmd_parse_launch(cmd_text, &err, &parse_ctx, 0);
        if(pipeline == NULL){
            av_print("*** ERROR: Create pipleline[%s] FAILED.\n", pipe_name);
            goto exit;
        }
        gst_element_set_name(pipeline, pipe_name);
        if (verbose) {
            g_signal_connect (pipeline, "deep-notify", G_CALLBACK (gst_object_default_deep_notify), NULL);
        }

//        scret = gst_element_set_state (pipeline, GST_STATE_READY);
//        if(scret == GST_STATE_CHANGE_FAILURE){
//            av_print("*** ERROR: set pipeline[%s] to GST_STATE_READY failed!!!\n", gst_element_get_name(pipeline));
//            exit(0);
//        }

        scret = gst_element_set_state (pipeline, GST_STATE_PAUSED);
        av_print("scret=%d\n", scret);
        switch(scret){
        case GST_STATE_CHANGE_FAILURE:
            av_print("*** ERROR: Pipeline[%s] set to GST_STATE_PAUSED failed!!!\n", gst_element_get_name(pipeline));
            res = -1;
            mdl_event_loop (pipeline, FALSE, GST_STATE_VOID_PENDING);
            goto end;
        case GST_STATE_CHANGE_SUCCESS:
            av_print ("Pipeline[%s] is PREROLLED ...\n", gst_element_get_name(pipeline));
            break;
        case GST_STATE_CHANGE_ASYNC:
            av_print ("Pipeline[%s] is PREROLLING ...\n", gst_element_get_name(pipeline));
            caught_error = mdl_event_loop (pipeline, FALSE, GST_STATE_PAUSED);
            if (caught_error) {
                av_print ("*** ERROR: Pipeline[%s] doesn't want to preroll.\n", gst_element_get_name(pipeline));
                goto end;
            }
//            state = GST_STATE_PAUSED;
        case GST_STATE_CHANGE_NO_PREROLL:
            av_print (_("Pipeline is live and does not need PREROLL ...\n"));
            is_live = TRUE;
            break;
        default:
            av_print("*** ERROR: [%d] Unkonwn GstStateChangeReturn\n", scret);
        }

        av_print("+*******************************\n");
        caught_error = mdl_event_loop (pipeline, FALSE, GST_STATE_PLAYING);
        av_print("-*******************************\n");
        if(caught_error) goto end;

        pipelist = g_slist_append(pipelist, pipeline);
    }

    for(ll = pipelist; ll; ll = ll->next){
        pipeline = ll->data;
        scret = gst_element_set_state (pipeline, GST_STATE_PLAYING);

        av_print("###Pipeline[%s] set to PLAYING scret=%d\n", gst_element_get_name(pipeline), scret);

        if(scret == GST_STATE_CHANGE_FAILURE){
            av_print("*** ERROR: set pipeline[%s] to GST_STATE_PLAYING failed!!!\n", gst_element_get_name(pipeline));
            exit(0);
        }else if (scret == GST_STATE_CHANGE_ASYNC){
//            caught_error = mdl_event_loop (pipeline, FALSE, GST_STATE_PLAYING);
//            if (caught_error) {
//                av_print ("*** ERROR: Pipeline[%s] doesn't want to GST_STATE_CHANGE_ASYNC.\n", gst_element_get_name(pipeline));
//                goto end;
//            }
        }else if (scret == GST_STATE_CHANGE_NO_PREROLL){
            av_print (_("Pipeline is live and does not need PREROLL ...\n"));
        }

    }


    __pid_t server_pid = mdl_create_server_process("node", "node ./httpserver.js --iport 8000 --oport 20120");


    g_print ("Running...\n");
//    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    mdl_event_loop(g_slist_nth(pipelist, 0)->data, TRUE, GST_STATE_PLAYING);

    //delink all appsink-->appsrc
    for(ll = parse_ctx.qlink_list; ll; ll = ll->next){
        qlink_t *link = ll->data;
        link->quit = TRUE;
    }


end:
    sprintf(cmd, "kill -s 15 %d", server_pid);
    ret = system(cmd);
    if(WEXITSTATUS(ret) != 0){
        //do nothing
    }

    for(ll = pipelist; ll; ll = ll->next){
        GstState state, pending;
        pipeline = ll->data;
        av_print ("Setting pipeline[%s] to PAUSED ...\n", gst_element_get_name(pipeline));
        gst_element_set_state (pipeline, GST_STATE_PAUSED);
        gst_element_get_state (pipeline, &state, &pending, GST_CLOCK_TIME_NONE);

        av_print (_("Setting pipeline to READY ...\n"));
        gst_element_set_state (pipeline, GST_STATE_READY);
        gst_element_get_state (pipeline, &state, &pending, GST_CLOCK_TIME_NONE);

        av_print (_("Setting pipeline to NULL ...\n"));
        gst_element_set_state (pipeline, GST_STATE_NULL);
        gst_element_get_state (pipeline, &state, &pending, GST_CLOCK_TIME_NONE);
    }

    av_print("\n");
    /* iterate mainloop to process pending stuff */
    while (g_main_context_iteration (NULL, FALSE));

    av_print("\n");

exit:
    for(ll = parse_ctx.qlink_list; ll; ll = ll->next){
        qlink_t *link = ll->data;
        link->appsink = NULL;
        link->appsrc  = NULL;
        gst_caps_unref(link->caps);
        g_mutex_free(link->mutex);
        g_queue_free(link->queue);

        free(link->cbf_appsrc);
        free(link->cbf_appsink);
    }
    for(ll = pipelist; ll; ll = ll->next){
        pipeline = ll->data;
        gst_object_unref (pipeline);
    }
    gst_alloc_trace_print_live ();
    g_hash_table_unref(section_table);
    gst_deinit ();
    mdl_syntax_uninit();

    return 0;
}

static EventLoopResult
mdl_event_loop (GstElement * pipeline, gboolean blocking, GstState target_state)
{
    GstBus *bus;
    GstMessage *message = NULL;
    EventLoopResult res = ELR_NO_ERROR;
    gboolean buffering = FALSE;
    gulong timeout_id;

    timeout_id = g_timeout_add (250, (GSourceFunc) check_intr, pipeline);

    bus = gst_element_get_bus (GST_ELEMENT (pipeline));


    while (TRUE) {
        message = gst_bus_poll (bus, GST_MESSAGE_ANY, blocking ? -1 : 0);

        if (message == NULL)
            goto exit;

        /* check if we need to dump messages to the console */
        if (0 || messages) {
            GstObject *src_obj;
            const GstStructure *s;
            guint32 seqnum;

            seqnum = gst_message_get_seqnum (message);

            s = gst_message_get_structure (message);

            src_obj = GST_MESSAGE_SRC (message);

            if (GST_IS_ELEMENT (src_obj)) {
                av_print ("Got message #%u from element \"%s\" (%s): ", (guint) seqnum, GST_ELEMENT_NAME (src_obj), GST_MESSAGE_TYPE_NAME (message));
            } else if (GST_IS_PAD (src_obj)) {
                av_print ("Got message #%u from pad \"%s:%s\" (%s): ", (guint) seqnum, GST_DEBUG_PAD_NAME (src_obj), GST_MESSAGE_TYPE_NAME (message));
            } else if (GST_IS_OBJECT (src_obj)) {
                av_print ("Got message #%u from object \"%s\" (%s): ", (guint) seqnum, GST_OBJECT_NAME (src_obj), GST_MESSAGE_TYPE_NAME (message));
            } else {
                av_print ("Got message #%u (%s): ", (guint) seqnum, GST_MESSAGE_TYPE_NAME (message));
            }

            if (s) {
                gchar *sstr;

                sstr = gst_structure_to_string (s);
                g_print ("%s\n", sstr);
                g_free (sstr);
            } else {
                g_print ("no message details\n");
            }
        }

//        av_print("[%s] RCV-MSG:%s ", gst_element_get_name(pipeline), gst_message_type_get_name(GST_MESSAGE_TYPE (message)));;
        switch (GST_MESSAGE_TYPE (message)) {
        case GST_MESSAGE_NEW_CLOCK:
        {
            GstClock *clock;

            gst_message_parse_new_clock (message, &clock);

            av_print ("New clock: %s\n", (clock ? GST_OBJECT_NAME (clock) : "NULL"));
            break;
        }
        case GST_MESSAGE_CLOCK_LOST:
            break;
        case GST_MESSAGE_EOS:{
            waiting_eos = FALSE;
            av_print (_("Got EOS from element \"%s\".\n"), GST_MESSAGE_SRC_NAME (message));
            goto exit;
        }
        case GST_MESSAGE_STATE_CHANGED:{
            GstState old, new, pending;

            gst_message_parse_state_changed (message, &old, &new, &pending);

//            g_print("    STATE_CHANGED: {%s ----> %s} pending=%s \n",
//                    gst_element_state_get_name(old), gst_element_state_get_name(new), gst_element_state_get_name(pending));

            /* we only care about pipeline state change messages */
            if (GST_MESSAGE_SRC (message) != GST_OBJECT_CAST (pipeline))
                break;

            /* dump graph for pipeline state changes */
            {
                gchar *dump_name = g_strdup_printf ("gst-launch.%s_%s",
                        gst_element_state_get_name (old),
                        gst_element_state_get_name (new));
                GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (pipeline),
                        GST_DEBUG_GRAPH_SHOW_ALL, dump_name);
                g_free (dump_name);
            }

            /* ignore when we are buffering since then we mess with the states
             * ourselves. */
            if (buffering) {
                av_print (_("Prerolled, waiting for buffering to finish...\n"));
                break;
            }

            /* if we reached the final target state, exit */
            if (target_state == GST_STATE_PAUSED && new == target_state)
                goto exit;

            /* else not an interesting message */
            break;
        }
        case GST_MESSAGE_BUFFERING:{
            gint percent;

            gst_message_parse_buffering (message, &percent);
            av_print ("%s %d% \r", gst_element_get_name(pipeline), percent);

            if (is_live) break;

            if (percent == 100) {
                /* a 100% message means buffering is done */
                buffering = FALSE;
                /* if the desired state is playing, go back */
                if (target_state == GST_STATE_PLAYING) {
                    av_print (_("Done buffering, setting pipeline to PLAYING ...\n"));
                    gst_element_set_state (pipeline, GST_STATE_PLAYING);
                } else
                    goto exit;
            } else {
                /* buffering busy */
                if (buffering == FALSE && target_state == GST_STATE_PLAYING) {
                    /* we were not buffering but PLAYING, PAUSE  the pipeline. */
                    av_print (_("Buffering, setting pipeline to PAUSED ...\n"));
                    gst_element_set_state (pipeline, GST_STATE_PAUSED);
                }
                buffering = TRUE;
            }
            break;
        }
        case GST_MESSAGE_REQUEST_STATE:
        {
            GstState state;
            gchar *name = gst_object_get_path_string (GST_MESSAGE_SRC (message));

            gst_message_parse_request_state (message, &state);

            av_print (_("Setting state to %s as requested by %s...\n"),
                    gst_element_state_get_name (state), name);

            gst_element_set_state (pipeline, state);

            g_free (name);
            break;
        }
        case GST_MESSAGE_APPLICATION:{
            const GstStructure *s;

            s = gst_message_get_structure (message);

            if (gst_structure_has_name (s, "GstLaunchInterrupt")) {
                /* this application message is posted when we caught an interrupt and
                 * we need to stop the pipeline. */
                av_print (_("Interrupt: Stopping pipeline ...\n"));
                res = ELR_INTERRUPT;
                goto exit;
            }
        }
        default:
            break;
        }
//        g_print("\n");
        if (message)
            gst_message_unref (message);
    }
exit:
    g_print("\n");
    {
        if (message)
            gst_message_unref (message);
        gst_object_unref (bus);
        g_source_remove (timeout_id);
        return res;
    }
}


static void
mdl_parse_args(int argc, char* argv[], GOptionEntry *options)
{
    GOptionContext *ctx;
    GError *err = NULL;

    ctx = g_option_context_new ("PIPELINE-DESCRIPTION");
    g_option_context_add_main_entries (ctx, options, GETTEXT_PACKAGE);
    g_option_context_add_group (ctx, gst_init_get_option_group ());
    if (!g_option_context_parse (ctx, &argc, &argv, &err)) {
      if (err)
        av_print ("Error initializing: %s\n", GST_STR_NULL (err->message));
      else
        av_print ("Error initializing: Unknown error!\n");
      exit (1);
    }
    g_option_context_free (ctx);
}

static GSList *
mdl_cmd_parse_text(const char* data)
{
    GSList *list = NULL;
    char *start, *p0, *p1, *end, *pp;
    gboolean bret;

    start = data;
    p0 = p1 = start;
    end = start + strlen(data);



    p0 = strstr(start, ">>");
    if(p0 == NULL){
        list = g_slist_append(list, g_string_new(start));
        return list;
    }

    p0 = strstr(start, ">>"); p1 = strstr(start, "@");

    if(p0 >= p1){
        av_print("*** ERROR: config flex not correct!!!\n");
        exit(0);
    }

    while(p0 < p1){ p1 ++; p0 = strstr(p1, ">>"); p1 = strstr(p1, "@");}
    pp = p1;

    while(p0){
        p0 = strstr(p1, ">>");
        p1 = strstr(p1, "@");
        if(p0 < p1){
            av_print("*** ERROR: config flex not correct!!!\n");
            exit(0);
        }

        p1 = p0 + 2;
    }

    char *buf = malloc(strlen(data));
    int size = 0;

    size = pp - start;
    memcpy(buf, start, size);
    buf[size] = 0;
    list = g_slist_append(list, g_string_new(buf));

    size = end - pp;
    memcpy(buf, pp, size);
    buf[size] = 0;
    list = g_slist_append(list, g_string_new(buf));

    free(buf);

    return list;
}

static __pid_t
mdl_create_server_process(char* file, char* _args)
{
    int ret;
    __pid_t pid;
    pid = fork();
    if(pid > 0){
        av_print("Create server process SUCCESSFUL!!!\n");
    }else if(pid == 0){
        const int MAX_PARAMS = 32;
        char *strecvp[MAX_PARAMS];
        int i;
        char *p0, *p1, *end, *str = strdup(_args);

        memset(strecvp, 0, sizeof(char*) * MAX_PARAMS);

        gboolean has_word = FALSE;
        p0 = p1 = str;
        end = str + strlen(str);
        i = 0;
        //NOTICE: "p1 <= end" is must.
        while( p1 <= end){
            if(!has_word && *p1 != ' ') has_word  = TRUE;

            if(has_word && (*p1 == ' ' || *p1 == 0) ){
                *p1 = 0;
                strecvp[i ++] = p0;

                av_print("    p0=%s\n", p0);
                p0 = p1 + 1;

                has_word = FALSE;
            }

            p1 ++;
        }

        ret = execvp(file, strecvp);

        SAFE_FREE(str);
        av_print("*** ERROR:[%s] ret=%d start server process FAILED!!!\n", strerror(errno), ret);
        exit(1);
    }else{
        av_print("*** ERROR:[%s] create server process FAILED!!!\n", strerror(errno));
        exit(1);
    }

    return pid;
}

static void
sigint_restore (void)
{
  struct sigaction action;

  memset (&action, 0, sizeof (action));
  action.sa_handler = SIG_DFL;

  sigaction (SIGINT, &action, NULL);
}

static void
sigint_handler_sighandler (int signum)
{
    g_print ("Caught interrupt -- ");

    if (waiting_eos) {
        waiting_eos = FALSE;
    } else {
        sigint_restore ();
    }
    av_print("\n");
    caught_intr = TRUE;
}

static void
sigint_setup (void)
{
    struct sigaction action;


    memset (&action, 0, sizeof (action));
    action.sa_handler = sigint_handler_sighandler;

    int ret = sigaction (SIGINT, &action, NULL);

    av_print("=========================ret=%d\n", ret);
}

static gboolean
check_intr (GstElement * pipeline)
{
    if (!caught_intr) {
        return TRUE;
    } else {
        caught_intr = FALSE;
        g_print ("handling interrupt.\n");

        /* post an application specific message */
        gst_element_post_message (GST_ELEMENT (pipeline),
                gst_message_new_application (GST_OBJECT (pipeline),
                        gst_structure_new ("GstLaunchInterrupt",
                                           "message", G_TYPE_STRING, "Pipeline interrupted", NULL)));

        /* remove timeout handler */
        return FALSE;
    }
}

static void
free_key(gpointer data)
{
    free(data);
}

static void
free_value(gpointer data)
{
    free(data);
}

static gboolean
mdl_file_exist(const char* filename)
{
    int fd = open(filename, O_RDONLY);
    if(fd == -1){
        return FALSE;
    }else{
        close(fd);
        return TRUE;
    }
}
