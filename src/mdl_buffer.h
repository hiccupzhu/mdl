/*
 * mdl_buffer.h
 *
 *  Created on: Nov 12, 2013
 *      Author: szhu
 */

#ifndef MDL_BUFFER_H_
#define MDL_BUFFER_H_

#include <stdint.h>

typedef struct _GsByte{
    uint8_t * mBuf;
    uint8_t * mBufPtr;
    uint8_t * mBufEnd;
}GsByte;

void mdl_buffer_alloc_w8(GsByte* b, char ch);

#endif /* MDL_BUFFER_H_ */
