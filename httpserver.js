var fs = require('fs');
var net = require('net');
var http = require('http');

var g_iport = 0;
var g_oport = 0;

//////////////////////////////////////////////////////////////////////////////////////////

function usage(){
    console.log("Usage:");
    console.log("    node <js-script> [options]");
    console.log("    [options]:");
    console.log("        --iport: tcp input port");
    console.log("        --oport: http output listenning port");
    console.log("e.g.");
    console.log("    node httpserver.js --iport 8000 --oport 20120");
}


if (process.argv.length <=2 ){
    console.log("Arguments too few.");
    console.log("    Please use '-h' or '--help' get help.");
    process.exit(); 
}else{
    //set i=2 to skip the args: "node" and "js-script"
    for(var i = 2; i < process.argv.length; i ++){
        var val = process.argv[i];
        
        if(val == "--help" || val == "-h"){
            usage();
            process.exit();
        }
        
        if(val == "--iport"){
            g_iport = parseInt(process.argv[i + 1]);
            console.log(val + '= ' + g_iport);
            
            i ++;
        }else if(val == "--oport"){
            g_oport = parseInt(process.argv[i + 1]);
            console.log(val + '= ' + g_oport);
            
            i ++;
        }else{
            console.log("*** ERROR: '" + val + "'Unknown argument.");
            process.exit();
        }
    }
}

if( g_iport == 0 ){
    console.log('*** ERROR: "--iport" must be set.');
    process.exit();
}
if( g_oport == 0){
    console.log('*** ERROR: "--oport" must be set.');
    process.exit();
}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


//http_server
//////////////////////////////////////////////////////////////////////////////////////////
var server = http.createServer();
server.on('request', function (request, response) {
    console.log("### request.url: " + request.url);
    console.log(request.path);
    console.log("==" + eval(request.headers));
    
    var head_option = {};
    
    if(request.url != "/stream"){
        head_option = {
            'Connection': 'close',
            'Content-type': 'text/html'
        };
        response.writeHead(200, head_option);
        response.write("Unsupported the path:" + request.url);
        response.end();
        return ;
    }else{
        head_option = {
            'Content-Type': 'video/mpegts',
            'Connection':   'close',
            'Transfer-Encoding': 'chunked'
        };
    }
    response.writeHead(200, head_option);
    
    
    var tcp_client = net.connect({port: g_iport, host: '192.168.2.8'},
        function() { //'connect' listener
          console.log('client connected');
    });

    tcp_client.on('data', function(chunk) {
        bret = response.write(chunk);
        /*Returns true if the entire data was flushed successfully
         * to the kernel buffer. Returns false if all or
         * part of the data was queued in user memory.
         */
    }).on('end', function() {
        console.log('client end');
        response.end();
    }).on('error', function() {
        console.log('client error');
        response.end();
    }).on('close', function() {
        console.log('client close');
    }).on('drain', function() {
        console.log('client drain');
    });
    
    //response.write("Welcome to Node.js!");
    //response.end();
});

server.setTimeout(2000, function () { 
    console.log('server timeout');
});

console.log('Listening: ' + g_oport);
server.listen(g_oport);

server.on('connection', function (socket) { 
    console.log('server connection');
});


server.on('connect', function (request, socket, head) { 
    console.log('server connect');
});


server.on('close', function () { 
    console.log('server close');
});


server.on('error', function () { 
    console.log('server error');
});


server.on('clientError', function (exception, socket) { 
    console.log(exception);
    socket.unref();
    socket.destroy();
});

//tcp_client
//////////////////////////////////////////////////////////////////////////////////////////
